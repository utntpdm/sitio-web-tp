<?php
session_start();
if (!isset($_SESSION['id_usuario'])) {
    header("Location: ../html/login.html");
}
?>


<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Mandale Fruta | Inicio </title>
    <link rel="stylesheet" href="../css/style.css">
  </head>
  <body>
    <header>
      <div>
         <div><img id="logo" src="../img/Mandale.png" title="Mandale Fruta" alt="Logo de Mandale Fruta"></div>
         <div><h1>Mandale Fruta, tu verdulería online</h1></div>
      </div>
      <nav>
        <ul>
          <li><a href="#botonera" title=""><img src="../img/home.png" title="Ir a Inicio" alt="Ir a Inicio"></a></li>
          <li><a href="logout.php"><img src="../img/logout.png" title="Salir" alt="Logout"></a></li>
        </ul>
      </nav>
    </header>

    <div id ="bienvenido">
      <?php 
      echo "<h2>Bienvenido ".$_SESSION["nombre"]."</h2>";
      ?>
    </div>


    <section>
      <h3>Menu principal</h3>
      <div id="botonera">
        <ul>
          <li id="primercuadrado"><br/></li>
          <li id="segundocuadrado"><a href="#nosotros"> ¿Quiénes somos?</a></li>
          <li id="tercercuadrado"><a href="#pedido"> Realizá tu pedido</a></li>
          <li id="cuartocuadrado"><a href="#catalogo"> Enviá tu consulta</a></li>
        </ul>
      </div>
    </section>

    <div class = "hacerpedidos">
      <div id = "pedidos">
        <h2> Pedidos</h2>
          
              <?php
          $sql = "select * from productos";
          $host_db = "localhost";
          $user_db = "root";
          $pass_db = "";
          $db_name = "verduleria";
          $tbl_name = "productos";

          $conexion = mysqli_connect($host_db, $user_db, $pass_db, $db_name);

          $resultado = mysqli_query($conexion, $sql);

              while($unRegistro = mysqli_fetch_array($resultado)) {
                  echo "<form action='../php/procesa_home.php' name='opciones' method='post'>";
                  echo "<input type='hidden' name='id[]' value='$unRegistro[id]' /> ";
                  echo "<h2>$unRegistro[nombre]</h2> <br>";
                  echo "<input name='kilos[]' type='number' required='required' min='0' max='5' value='0'>";
                  echo "kilos <br>";
                  echo "<h3>Precio</h3> <input type= 'number' name='precio[]' value= $unRegistro[precio] readonly />";
                  // echo "</form>";

          }
          ?>
          <input type='submit' value='Guardar' />
          </form>
      </div>      
  <div>
    <h2> ultimos pedidos </h2>

    <?php
        $sql = "SELECT * FROM pedido WHERE id_usuario = $_SESSION[id_usuario] ORDER BY id DESC LIMIT 10 ";
        $host_db = "localhost";
        $user_db = "root";
        $pass_db = "";
        $db_name = "verduleria";
        $tbl_name = "pedido";

        $conexion = mysqli_connect($host_db, $user_db, $pass_db, $db_name);

        $resultado = mysqli_query($conexion, $sql);
        //$resultado_valor = mysqli_query($conexion, "SELECT sum(valor) total FROM pedido INNER JOIN detalle on detalle.id_pedido = pedido.id WHERE id_usuario = $_SESSION[id_usuario] ");
    ?> 
      <table>
        <thead>
          <tr>
            <th>Numero</th>
            <th>Fecha</th>
            <th>Producto</th>
          </tr>
        </thead>
    <?php
        while($unRegistro = mysqli_fetch_array($resultado)) {
          $resultado_valor = mysqli_query($conexion, "SELECT sum(valor) total from detalle  WHERE id_pedido = $unRegistro[id] ");
         echo "<tr>";
         echo "<td>$unRegistro[id]</td>";
         echo "<td>$unRegistro[fecha]</td>";
         if ($otroRegistro = mysqli_fetch_array($resultado_valor)){
            echo "<td>$otroRegistro[total]</td>";
          }
         echo "</tr>";
          
        }?>

    </table>
   </div> 
 </div>

  <Article id="nosotros">

  <div id="headabout">
    <img src="../img/Mandale.png" title="Mandale Fruta" alt="Logo de Mandale Fruta">
    <h3 id="titulomandalefruta">MANDALE FRUTA</h3>
  </div>

  <div id="about">
    

    <p>Somos una empresa innovadora dedicada a la producción y comercialización de alimentos. Nuestros productos se caracterizan por ser sanos y naturales, pero por sobre todo prácticos. Procuramos generar hábitos de alimentación saludables por eso nuestro foco se ve centrado en facilitar alimentos listos para su consumo. De este modo nuestros clientes podrán ahorrar tiempo al momento de la compra y de la cocina, recibiendo productos en la comodidad de su domicilio junto con flamantes recetas y originales métodos de cocción.</p><br/>

    <p><strong>Misión:</strong> Ser referentes en la comercialización de alimentos ofreciendo productos de excelente calidad, conveniencia y a través de un servicio diferenciado y personalizado en todo momento, desde el pedido hasta la entrega final.</p><br/>

    <p><strong>Visión:</strong> <em>MANDALE FRUTA</em> © será reconocido como la mejor experiencia de compra de productos frescos y listos para consumo en Rosario y la zona.</p><br/>

    <h3><strong>Valores: </strong></h3><br/>

      <ul>
        <li>Pasión por lo que hacemos</li>
        <li>Actitud de aprendizaje y superación constante</li>
        <li>Superar las expectativas del consumidor</li>
        <li>Garantizar máxima calidad</li>
      </ul><br/>

    <p><em>MANDALE FRUTA</em> © garantiza la máxima calidad de sus productos desde el origen hasta la entrega final. Partiendo del abastecimiento, teniendo contacto con diferentes productores, pasando luego por procesos rigurosos de higiene y seguridad, hasta la entrega final. Consecuentemente nuestros alimentos cumplen con los estándares deseados avalando frescura y calidad en todo momento.</p><br/>

    <p>En nuestra página Web podrá encontrar funciones prácticas para poder generar su compra, solicitando su pedido y generando una cuenta personal, conozca todos los productos que tenemos para ofrecerle. Viva la innovadora experiencia de un servicio personalizado y diferenciado.</p><br/>

    <h3 id="h3articlebottom"><em>"MANDALE FRUTA: SIMPLE, FRESCO y ONLINE"</em></h3>
    </div>
  </Article>

  <section id="pedido">
  <div>
    <div>
      <div class="ejercicio36">
        <img src ="../img/gajonaranja.png" alt= "gajonaranja"><h2>Tu consulta</h2></img>
      <form action="../php/formulario.php" method="post">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" id="nombre" name="nombre" placeholder="Nombre" />
        </div>

        <div>
            <label for="email">Email</label>
            <input type="text" id="email" name="email" placeholder="su email" />
        </div>

        <div>
            <label for="mensaje">Mensaje</label>
            <textarea name="mensaje" id="mensaje" cols="30" rows="10" placeholder="ingrese su mensaje"></textarea>
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>
    </form>
      </div>
    </div>
  </div>
</section>

    <footer id = "footerhome">
      <div>
        <ul>
          <li>
            <a href="https://www.facebook.com/">
              <img src="../img/facebook.png" title="Mandale Fruta Facebook Oficial" alt="MF en Facebook">
            </a>
          </li>
          <li>
            <a href="https://www.instagram.com/">
              <img src="../img/instagram.png" title="Mandale Fruta Instagram Oficial" alt="MF en Instagram">
            </a>
          </li>
          <li><a href="https://twitter.com/?lang=es"><img src="../img/twitter.png" title="Mandale Fruta Twitter Oficial" alt="MF en Twitter"></a></li>
        </ul>
      </div>
      <div>
    	<span>Copyright © 2019. Todos los derechos reservados.</span>
    </div>
    </footer>
  </body>
</html>