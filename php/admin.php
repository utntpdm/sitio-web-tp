<?php
session_start();
if (!isset($_SESSION['id_usuario'])) {
	header("Location: ../html/login.html");
}

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Mandale Fruta | Administrador </title>
    <link rel="stylesheet" href="../css/style.css">
  </head>
  <body>
    <header>
      <div>
         <div><img id="logo" src="../img/Mandale.png" title="Mandale Fruta" alt="Logo de Mandale Fruta"></div>
         <div><h1>Mandale Fruta, tu verdulería online</h1></div>
      </div>
      <nav>
        <ul>
          <li><a href="../html/index.html" title=""><img src="../img/home.png" title="Ir a Inicio" alt="Ir a Inicio"></a></li>
      </nav>
    </header>

	<body>
	<div>
    <form action="productoAlta.php" method="post">
      <div class="form-group">
        <label for="text1" class="col-4 col-form-label">Nombre</label> 
        <div class="col-8">
          <input id="text1" name="nombre" type="text" class="form-control" required="required">
        </div>
      </div>
      <div class="form-group">
        <label for="textarea" class="col-4 col-form-label">Descripción</label> 
        <div class="col-8">
          <textarea id="textarea" name="detalle" cols="40" rows="5" class="form-control"></textarea>
        </div>
      </div>
      <div class="form-group">
      <label class="custom-file">
  		<input type="file" id="file" class="custom-file-input" disabled="Proximamente">
  		<span class="custom-file-control">Subir Foto</span>
  	</label>
      </div>
      <div class="form-group">
        <label for="number" class="col-4 col-form-label">Precio</label> 
        <div class="col-8">
          <input id="number" name="precio" type="number" class="form-control" required="required" min="1">
        </div>
      </div> 
      <div class="form-group">
        <div class="offset-4 col-8">
          <button name="submit" type="submit" class="boton">Enviar</button>
        </div>
      </div>
    </form>

	</div>

	<div>
		<h2> baja de productos </h2>

	<?php
		$sql = "select * from productos";
		$host_db = "localhost";
		$user_db = "root";
		$pass_db = "";
		$db_name = "verduleria";
		$tbl_name = "productos";

		$conexion = mysqli_connect($host_db, $user_db, $pass_db, $db_name);

		$resultado = mysqli_query($conexion, $sql);
	?>
	<table>
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Precio</th>
				<th>Accion</th>
			</tr>
		</thead>
	<?php
		while($unRegistro = mysqli_fetch_array($resultado)) {
			echo "<tr>";
			echo "<td>$unRegistro[nombre]</td>";
			echo "<td>$unRegistro[precio]</td>";
			echo "<td><a href='borrar.php?id=$unRegistro[id]'>Borrar</a></td>";
			echo "</tr>";
			
		}


		?>
	</table>
	</div>
	  <div>
        <h2> MODIFICACION ARTICULOS</h2>
        
      <?php
        $sql = "select * from productos";
        $host_db = "localhost";
        $user_db = "root";
        $pass_db = "";
        $db_name = "verduleria";
        $tbl_name = "productos";

        $conexion = mysqli_connect($host_db, $user_db, $pass_db, $db_name);

        $resultado = mysqli_query($conexion, $sql);

            while($unRegistro = mysqli_fetch_array($resultado)) {
                echo "<form action='modifica.php' method='post'>";
                echo "<input type='hidden' name='id' value='$unRegistro[id]' /> ";
                echo "<input type='text' name='nombre' placeholder= '$unRegistro[nombre]'>";
                echo "<input type='submit' value='Guardar' />";
                echo "</form>";

        }
        ?>

        </form>
    </div>

    <footer>
      <div>
        <ul>
          <li>
            <a href="https://www.facebook.com/">
              <img src="../img/facebook.png" title="Mandale Fruta Facebook Oficial" alt="MF en Facebook">
            </a>
          </li>
          <li>
            <a href="https://www.instagram.com/">
              <img src="../img/instagram.png" title="Mandale Fruta Instagram Oficial" alt="MF en Instagram">
            </a>
          </li>
          <li><a href="https://twitter.com/?lang=es"><img src="../img/twitter.png" title="Mandale Fruta Twitter Oficial" alt="MF en Twitter"></a></li>
        </ul>
      </div>
      <div>
      <span>Copyright © 2019. Todos los derechos reservados.</span>
    </div>
    </footer>
  </body>
</html>