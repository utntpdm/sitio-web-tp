<?php
session_start();
unset($_SESSION['id_usuario']);
unset($_SESSION['nombre']);
unset($_SESSION['email']);
session_destroy();

header("Location: ../html/index.html");
exit;
?>