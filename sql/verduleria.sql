-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2019 at 12:25 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `verduleria`
--

-- --------------------------------------------------------

--
-- Table structure for table `detalle`
--

CREATE TABLE `detalle` (
  `id` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_productos` int(11) NOT NULL,
  `cantidad` int(10) UNSIGNED NOT NULL,
  `valor` decimal(60,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `detalle`
--

INSERT INTO `detalle` (`id`, `id_pedido`, `id_productos`, `cantidad`, `valor`) VALUES
(2, 26, 1, 2, '0.00'),
(3, 26, 4, 4, '0.00'),
(4, 26, 5, 4, '0.00'),
(5, 26, 6, 0, '0.00'),
(6, 26, 7, 0, '0.00'),
(7, 28, 1, 2, '0.00'),
(8, 28, 4, 4, '0.00'),
(9, 28, 5, 4, '0.00'),
(10, 28, 6, 0, '0.00'),
(11, 28, 7, 0, '0.00'),
(12, 30, 1, 0, '123.00'),
(13, 30, 4, 0, '1234.00'),
(14, 30, 5, 3, '134.00'),
(15, 30, 6, 2, '222.00'),
(16, 30, 7, 2, '255.00'),
(17, 31, 1, 3, '123.00'),
(18, 31, 4, 0, '1234.00'),
(19, 31, 5, 0, '134.00'),
(20, 31, 6, 2, '222.00'),
(21, 31, 7, 3, '255.00'),
(22, 32, 1, 0, '123.00'),
(23, 32, 4, 0, '1234.00'),
(24, 32, 5, 3, '134.00'),
(25, 32, 6, 3, '222.00'),
(26, 32, 7, 3, '255.00'),
(27, 33, 1, 0, '123.00'),
(28, 33, 4, 0, '1234.00'),
(29, 33, 5, 0, '134.00'),
(30, 33, 6, 0, '222.00'),
(31, 33, 7, 0, '255.00'),
(32, 34, 1, 2, '123.00'),
(33, 34, 4, 2, '1234.00'),
(34, 34, 5, 2, '134.00'),
(35, 34, 6, 2, '222.00'),
(36, 34, 7, 2, '255.00'),
(37, 35, 1, 2, '246.00'),
(38, 35, 4, 3, '3702.00'),
(39, 35, 5, 4, '536.00'),
(40, 35, 6, 5, '1110.00'),
(41, 35, 7, 5, '1275.00');

-- --------------------------------------------------------

--
-- Table structure for table `pedido`
--

CREATE TABLE `pedido` (
  `id` int(10) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pedido`
--

INSERT INTO `pedido` (`id`, `id_usuario`, `fecha`) VALUES
(1, 11, '0000-00-00'),
(2, 11, '0000-00-00'),
(3, 11, '2019-03-10'),
(4, 11, '2019-03-10'),
(5, 11, '2019-03-10'),
(6, 11, '2019-03-10'),
(7, 11, '2019-03-10'),
(8, 11, '2019-03-10'),
(9, 11, '2019-03-10'),
(10, 11, '2019-03-10'),
(11, 11, '2019-03-10'),
(12, 11, '2019-03-10'),
(13, 11, '2019-03-10'),
(14, 11, '2019-03-10'),
(15, 12, '2019-03-10'),
(16, 12, '2019-03-10'),
(17, 11, '2019-03-10'),
(18, 11, '2019-03-10'),
(19, 11, '2019-03-11'),
(20, 11, '2019-03-11'),
(21, 11, '2019-03-11'),
(22, 11, '2019-03-11'),
(23, 11, '2019-03-11'),
(24, 11, '2019-03-11'),
(25, 11, '2019-03-11'),
(26, 11, '2019-03-11'),
(27, 11, '2019-03-11'),
(28, 11, '2019-03-11'),
(29, 11, '2019-03-11'),
(30, 11, '2019-03-11'),
(31, 11, '2019-03-11'),
(32, 11, '2019-03-11'),
(33, 11, '2019-03-11'),
(34, 11, '2019-03-11'),
(35, 11, '2019-03-11');

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id` int(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `imagen` varchar(150) DEFAULT NULL,
  `descripcion` varchar(400) DEFAULT NULL,
  `precio` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `imagen`, `descripcion`, `precio`) VALUES
(1, 'manzanas', NULL, 'El fruto del manzano', 123),
(4, 'banana', NULL, 'Del platano', 1234),
(5, 'peras', NULL, 'Del peral', 134),
(6, 'Lechugas', NULL, 'De la tierra', 222),
(7, 'tomates', NULL, 'Del tomatero', 255);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `email`, `password`, `apellido`, `date`) VALUES
(11, 'Dante_malosetti', 'dante.malosetti@davinci.edu.ar', '$2y$10$zaBZSthKmk.MnCPw5LRA5O9.QAIkSfHltGG0dJWNB84VXCjVKUBI2', '', '2019-03-10'),
(12, 'Cecilia', 'chechu@gmail.com', '$2y$10$jzH0nXIrh37InMEdKFHjZuPeJ.dDG1Zsj9WRdjsoGHOwoxiqENgAm', '', '2019-03-10'),
(13, 'carlos', 'hhhh@hhhh.hhh', '$2y$10$4RMqQy4R0m7VH1oYNEk00.bHThH2/KnuWdfzeJVqEIV9qp/6.GuE6', '', '2019-03-10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detalle`
--
ALTER TABLE `detalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_productos` (`id_productos`),
  ADD KEY `id_pedido` (`id_pedido`);

--
-- Indexes for table `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `usuario` (`usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detalle`
--
ALTER TABLE `detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detalle`
--
ALTER TABLE `detalle`
  ADD CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`id_productos`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `detalle_ibfk_3` FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id`);

--
-- Constraints for table `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
